from django.contrib.auth.models import User
from rest_framework import serializers

from utilpass.models import KeyPass


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']


class KeyPassSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = KeyPass
        fields = ['key', 'passwodr', 'user']
