from django.contrib.auth.models import User
from rest_framework import viewsets

from utilpass.models import KeyPass
from utilpass.serializers import UserSerializer, KeyPassSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class KeyPassViewSet(viewsets.ModelViewSet):
    queryset = KeyPass.objects.all()
    serializer_class = KeyPassSerializer
